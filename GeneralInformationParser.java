import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class GeneralInformationParser {

	
	public int modalUang, waktu, energi, strengthAwal, charmAwal, brainAwal;
	
	public void ReadFile(){
		try{
			BufferedReader buffreader = new BufferedReader(new FileReader("GeneralInformation.txt"));
			String line    = "";
			line           = buffreader.readLine();
			String[] words = line.split(" ");
		   
			modalUang    = Integer.parseInt(words[0]);
			waktu        = Integer.parseInt(words[1]);
			energi       = Integer.parseInt(words[2]);
			strengthAwal = Integer.parseInt(words[3]);
			charmAwal    = Integer.parseInt(words[4]);
			brainAwal    = Integer.parseInt(words[5]);
			
			ai.modalUang    = modalUang;
			ai.waktu        = waktu;
			ai.energi       = energi;
			ai.strengthAwal = strengthAwal;
			ai.charmAwal    = charmAwal;
			ai.brainAwal    = brainAwal;
		   
		   line = buffreader.readLine();
		   int jumlahKandidat = Integer.parseInt(line);
		   ai.NumberOfCandidate = jumlahKandidat;
		   for (int i=0; i< jumlahKandidat; i++){
				int enlight,energy,max;
				String preq;
				int str, charm, brain;
			   
				line  = buffreader.readLine();
				words = line.split(" ");

				enlight = Integer.parseInt(words[0]);
				energy  = Integer.parseInt(words[1]);
				max     = Integer.parseInt(words[2]);
				if(words[3].equals("-")){
					preq = "";
				}else{
					preq    = words[3];
				}
				str     = Integer.parseInt(words[4]);
				charm   = Integer.parseInt(words[5]);
				brain   = Integer.parseInt(words[6]);
			   
			   	// TODO
			   	// Add ke list of kandidat

				Candidate C = new Candidate(enlight,energy,max,preq,str,charm,brain);
				//push ke ke array milik ai
				ai.arraycandidate.add(C);
		   }
		   line = buffreader.readLine();
		   int jumlahBarang = Integer.parseInt(line);
		   ai.NumberOfItems = jumlahBarang;

		   // Sedikit trik yang dipake untuk ngerename nama barang 
		   String kodebarang = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		   for (int i=0;i<jumlahBarang;i++){
			    String kode;
			    int harga, restock;
			   
				line  = buffreader.readLine();
				words = line.split(" ");
				kode  = words[0];

				harga   = Integer.parseInt(words[1]);
				restock = Integer.parseInt(words[2]);
			    // TODO
			    // Add ke list of barang
			    Items I = new Items(harga,restock);
			    Character kodebrg = kodebarang.charAt(i);
			    ai.arrayitem.put(kodebrg, I);
		   }
		}
		catch(IOException e){
		   System.out.println("File not found");
		}
	}
	
	public void DisplayGeneralInfo(){
		System.out.println("============");
		System.out.println("modalUang 		: "+this.modalUang);
		System.out.println("waktu 			: "+this.waktu);
		System.out.println("energi 			: "+this.energi);
		System.out.println("strengthAwal 	\t: "+this.strengthAwal);
		System.out.println("charmAwal 		: "+this.charmAwal);
		System.out.println("brainAwal 		: "+this.brainAwal);
		System.out.println("============");
	}

	// Cuma dipake untuk ngecek doang MAIN program tetep di ai.java
	// public static void main(String[] args) {
	// 	GeneralInformationParser GIP = new GeneralInformationParser();
	// 	GIP.ReadFile();
	// 	GIP.DisplayGeneralInfo();
	// }
	
}
