// package
import java.util.*;

public class validator {
	/*
	 * Array Valid : 
	 * Representasi Alfabet/Huruf Apa saja yang valid untuk waktu ke-"n"
	 * Karena tidak setiap tempat/kandidat bisa ditemui pada waktu ke-"n"
	 * ============
	 * MinimumPlaceVisit:
	 * Berapa kali nanto harus mengunjungi gym/cafe/university untuk mencapai attribut minimum
	 * MinimumPlaceVisit[KodeKandidat][Kodetempat]
	 * Kode tempat 1 = Gymnasium
	 * Kode tempat 2 = Cafe
	 * Kode tempat 3 = University
	 * ============ 
	 * */
	public static String[] ArrayValid;
	private int[][] MinimumPlaceVisit;  
	
	// Constructor
	public void Validator(){
		ArrayValid = new String[ai.waktu * 7 * 12 ];
		MinimumPlaceVisit = new int[ai.NumberOfCandidate][3];
	}
	
	// FUngsi utama
	public boolean validasi(String s){
		return false;
	} 	
	
	
	/* *
	 * BuildValidArray
	 * */
	public void BuildValidArray(){
		// Jadwal Schedule
		
		int jumlahjam = ai.waktu * 7 * 12 ;
		for (int i = 0; i < jumlahjam; i++) {
			int j = i % jumlahjam;
			
			// Masukan valid 
			
			// Representasi dari Mall itu di index pertama array schedule
			if(PlaceScheduleParser.schedule[0][j]==1){
				ArrayValid[i] = ArrayValid[i] + "m";
			}
			
			// Representasi dari Gymnasium itu di index kedua array schedule
			if(PlaceScheduleParser.schedule[1][j]==1){
				ArrayValid[i] = ArrayValid[i] + "g";
			}
			
			// Representasi dari Cafe itu di index ketiga array shcedule
			if(PlaceScheduleParser.schedule[2][j]==1){
				ArrayValid[i] = ArrayValid[i] + "c";
			}
			
			// Representasi dari Jadwal University Yang alvailable ada di index keempat array schedule
			if(PlaceScheduleParser.schedule[3][j]==1){
				ArrayValid[i] = ArrayValid[i] + "u";
			}
			
			// Representasi dari Jadwal Candidate 
			for (int k = 0; k < ai.NumberOfCandidate; k++) {
				// Better check if it is calculated minimum start from
				if(CandidateScheduleParser.schedule[k][j]==1){
					ArrayValid[i] = ArrayValid[i] + Integer.toString(k);
				}
			}
		}
	}

	
	public void BuildValidSkill(){
		Place gym  = ai.arrayplace.get(Place.KODEGYM);
		Place cafe = ai.arrayplace.get(Place.KODECAFE);
		Place univ = ai.arrayplace.get(Place.KODEUNIV);
		for (int i = 0; i < ai.NumberOfCandidate; i++) {
			// Menghitung seberapa cepat candidate tersebut dapat ditemui berasarkan
			// syarat minimal Charm / Brain / Strength
			Candidate C = ai.arraycandidate.get(i);
			
			// Menghitung berapa kali kandidat harus ke gym untuk mencapai 
			// Strength Minimum yang dibutuhkan untuk menemui Kandidat ke - i
			MinimumPlaceVisit[i][Place.KODEGYM] = C.StrengthNeeded / gym.AttributeAdded;  
			
			// Menghitung berapa kali kandidat harus ke cafe untuk mencapai 
			// Charm Minimum yang dibutuhkan untuk menemui Kandidat ke - i
			MinimumPlaceVisit[i][Place.KODECAFE] = C.CharmNeeded / cafe.AttributeAdded;
					
			// Menghitung berapa kali kandidat harus ke university untuk mencapai 
			// Body Minimum yang dibutuhkan untuk menemui Kandidat ke - i
			MinimumPlaceVisit[i][Place.KODEUNIV] = C.BrainNeeded / univ.AttributeAdded;
		}
	}
	/**
	* Validasi_jadwal_tempat
	*	Input  : String representasi solusi
	* 	Output : Vector of integer, dimana integer tersebut merepresentasikan pada jam ke-berapa Nanto Tidak valid
	*/
	public ArrayList<Integer> validasi_jadwal_tempat(String s){
		
		// Notes cek juga untuk waktu kalau mausuk ke pekan ke dua
		CandidateScheduleParser candidate_schedule = new CandidateScheduleParser();
		candidate_schedule.ReadFile();
		
		
		Boolean valid = true;
		ArrayList<Integer> VO = new ArrayList<Integer>();
		for (int i=0; i < s.length() ; i++) {
			char c = s.charAt(i);
			switch (c) {
				case 'g' : 
					// Return dari Class Schedule  yang menampung jadwal table boolean true/false
					break;
				case 'm' : 
					break;
				case 'c' : 
					break;
				case 'u' : 
					break;
			}
		}
		return VO;
	}

	/**
	* validasi_jadwal_kandidat
	*	Input: String representasi calon solusi
	*	Output : Vector of integer dimana integer tersebut merepresentasikan pada jam ke berapa validasi ini gagal.
	*/
	public ArrayList<Integer> validasi_jadwal_kandidat(String s){
		Boolean valid = true;
		ArrayList<Integer> VO = new ArrayList<Integer>();
		for (int i=0; i < s.length() ; i++) {
			char c = s.charAt(i);
			switch (c) {
				case 'g' : 
					// Return dari Class Schedule  yang menampung jadwal table boolean true/false
					break;
				case 'm' : 
					break;
				case 'c' : 
					break;
				case 'u' : 
					break;
			}
		}
		return VO;
	}



	/*
	* Mulai ke bawah fungsi validasi akan dipecah pecah menjadi beberapa modul kecil 
	**/
	
	private int CountCharmUntill(){
		return 0;
	}

	private int CountBrainUntill(){
		return 0;
	}

	private int CountStrengthUntill(){
		return 0;
	}

	private int CountUangUntill(){
		return 0;
	}

	public ArrayList<Integer> validasi_prerequisite_barang(){
		ArrayList<Integer> O = new ArrayList<>();
		return O;
	}

	public ArrayList<Integer> validasi_prerequisite_charm(){
		ArrayList<Integer> O = new ArrayList<>();
		return O;
	}
	
	public ArrayList<Integer> validasi_prerequisite_brain(){
		ArrayList<Integer> O = new ArrayList<>();
		return O;
	}
	
	public ArrayList<Integer> validasi_prerequisite_strength(){
		ArrayList<Integer> O = new ArrayList<>();
		return O;
	}
	
	public ArrayList<Integer> validasi_maximal_jam_candidate(){
		ArrayList<Integer> O = new ArrayList<>();
		return O;
	}

	public ArrayList<Integer> validasi_jumlah_uang(){
		ArrayList<Integer> O = new ArrayList<>();
		return O;
	}


	
}