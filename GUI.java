import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class GUI extends JFrame {

	public JLabel lblNewLabel;
	public JLabel lblNewLabel_1;
	public JLabel lblNewLabel_2;
	public JLabel lblNewLabel_3;
	public JLabel lblNewLabel_4;
	public JLabel lblNewLabel_5;
	public JLabel lblNewLabel_6;
	public JLabel text;
	public String solusi;
	static int counter = 0;
	/**
	 * Create the panel.
	 */
	public GUI(){}
	public GUI(String s) {
		//ai a = new ai();
		
		solusi = s;
		getContentPane().setLayout(null);
		

		lblNewLabel = new JLabel("mall");
		lblNewLabel.setIcon(new ImageIcon(getClass().getResource("/asset/mall.jpg")));
		lblNewLabel.setBounds(10, 15, 286, 196);lblNewLabel.setVisible(false);
		getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("univ");
		lblNewLabel_1.setIcon(new ImageIcon(getClass().getResource("/asset/university.jpg")));
		lblNewLabel_1.setBounds(306, 15, 235, 225);lblNewLabel_1.setVisible(false);
		getContentPane().add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("cafe");
		lblNewLabel_2.setIcon(new ImageIcon(getClass().getResource("/asset/cafe.jpg")));
		lblNewLabel_2.setBounds(15, 246, 122, 105);lblNewLabel_2.setVisible(false);
		getContentPane().add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("prereq");
		lblNewLabel_3.setIcon(new ImageIcon(getClass().getResource("/asset/prereq.jpg")));
		lblNewLabel_3.setBounds(152, 246, 230, 194);lblNewLabel_3.setVisible(false);
		getContentPane().add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("girlhouse");
		lblNewLabel_4.setIcon(new ImageIcon(getClass().getResource("/asset/girlhouse.jpg")));
		lblNewLabel_4.setBounds(397, 246, 225, 198);lblNewLabel_4.setVisible(false);
		getContentPane().add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("gym");
		lblNewLabel_5.setIcon(new ImageIcon(getClass().getResource("/asset/gym.jpg")));
		lblNewLabel_5.setBounds(637, 246, 312, 199);lblNewLabel_5.setVisible(false);
		getContentPane().add(lblNewLabel_5);
		
		lblNewLabel_6 = new JLabel("donothing");
		lblNewLabel_6.setIcon(new ImageIcon(getClass().getResource("do_nothing.png")));
		lblNewLabel_6.setBounds(556, 15, 238, 187);lblNewLabel_6.setVisible(false);
		getContentPane().add(lblNewLabel_6);

		
		
		 text = new JLabel("perjalanan dimulai....."); //keterangan langkah2 nanto
		text.setBounds(5, 383, 190, 23);
		getContentPane().add(text); 

		
		/*final JButton btnNewButton = new JButton("animation");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println(solusi.substring(counter, counter));
						if (counter<solusi.length()){
							try{
							//Thread.sleep(500);
							switch (solusi.substring(counter, counter+1)){
							case "0" :{
								lblNewLabel.setVisible(false); // mall
								lblNewLabel_1.setVisible(false); // university
								lblNewLabel_2.setVisible(false); // cafe
								lblNewLabel_3.setVisible(false); // prerequisite kado
								lblNewLabel_4.setVisible(false); // rumah cewe
								lblNewLabel_5.setVisible(false); // gym
								lblNewLabel_6.setVisible(true); // *ga ngapa2in*
								text.setText("gak ngapa2in..");
								break;}
							case "1" :
							case "2" :
							case "3" :
							case "4" :
							case "5" :
							case "6" :
							case "7" :
							case "8" :
							case "9" : {
								lblNewLabel.setVisible(false); // mall
								lblNewLabel_1.setVisible(false); // university
								lblNewLabel_2.setVisible(false); // cafe
								lblNewLabel_3.setVisible(false); // prerequisite kado
								lblNewLabel_4.setVisible(true); // rumah cewe
								lblNewLabel_5.setVisible(false); // gym
								lblNewLabel_6.setVisible(false); // *ga ngapa2in*
								text.setText("kunjungin cewe ke "+solusi.substring(counter, counter+1));
								break;
							}
							case "m" :{
								lblNewLabel.setVisible(true); // mall
								lblNewLabel_1.setVisible(false); // university
								lblNewLabel_2.setVisible(false); // cafe
								lblNewLabel_3.setVisible(false); // prerequisite kado
								lblNewLabel_4.setVisible(false); // rumah cewe
								lblNewLabel_5.setVisible(false); // gym
								lblNewLabel_6.setVisible(false); // *ga ngapa2in*
								text.setText("Nanto pergi ke mall");
								break;}
							case "g" :{
								lblNewLabel.setVisible(false); // mall
								lblNewLabel_1.setVisible(false); // university
								lblNewLabel_2.setVisible(false); // cafe
								lblNewLabel_3.setVisible(false); // prerequisite kado
								lblNewLabel_4.setVisible(false); // rumah cewe
								lblNewLabel_5.setVisible(true); // gym
								lblNewLabel_6.setVisible(false); // *ga ngapa2in*
								text.setText("Nanto pergi ke gymnasium");
								break;}
							case "c" :{
								lblNewLabel.setVisible(false); // mall
								lblNewLabel_1.setVisible(false); // university
								lblNewLabel_2.setVisible(true); // cafe
								lblNewLabel_3.setVisible(false); // prerequisite kado
								lblNewLabel_4.setVisible(false); // rumah cewe
								lblNewLabel_5.setVisible(false); // gym
								lblNewLabel_6.setVisible(false); // *ga ngapa2in*
								text.setText("Nanto pergi ke cafe..");
								break;}
							case "u " :{
								lblNewLabel.setVisible(false); // mall
								lblNewLabel_1.setVisible(true); // university
								lblNewLabel_2.setVisible(false); // cafe
								lblNewLabel_3.setVisible(false); // prerequisite kado
								lblNewLabel_4.setVisible(false); // rumah cewe
								lblNewLabel_5.setVisible(false); // gym
								lblNewLabel_6.setVisible(false); // *ga ngapa2in*
								text.setText("Nanto pergi ke univ buat belajar");
								break;
							
							}
							case "A":
							case "B":
							case "C":
							case "D":
							case "E":
							case "F":
							case "G":
							case "H":
							case "I":
							case "J":
							case "K":
							case "L":
							case "M":
							case "N":
							case "O":
							case "P":
							case "Q":
							case "R":
							case "S":
							case "T":
							case "U":
							case "V":
							case "W":
							case "X":
							case "Y":
							case "Z":
							{
								lblNewLabel.setVisible(false); // mall
								lblNewLabel_1.setVisible(false); // university
								lblNewLabel_2.setVisible(false); // cafe
								lblNewLabel_3.setVisible(true); // prerequisite kado
								lblNewLabel_4.setVisible(false); // rumah cewe
								lblNewLabel_5.setVisible(false); // gym
								lblNewLabel_6.setVisible(false); // *ga ngapa2in*
								text.setText("beli kado yang berkode "+solusi.substring(counter, counter+1));
								break;
							}
							}}catch(Exception x){
								x.printStackTrace();
							}finally{
								counter++;	
							}
						}
					}
				
				
			
		});
		btnNewButton.setBounds(43, 500, 89, 23);
		getContentPane().add(btnNewButton);*/
		
		
		//this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 970, 600);
		
		Thread myThread;
		myThread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (counter<solusi.length()){
					//System.out.println("hi");
					try{
					//Thread.sleep(500);
					switch (solusi.substring(counter, counter+1)){
					case "0" :{
						lblNewLabel.setVisible(false); // mall
						lblNewLabel_1.setVisible(false); // university
						lblNewLabel_2.setVisible(false); // cafe
						lblNewLabel_3.setVisible(false); // prerequisite kado
						lblNewLabel_4.setVisible(false); // rumah cewe
						lblNewLabel_5.setVisible(false); // gym
						lblNewLabel_6.setVisible(true); // *ga ngapa2in*
						text.setText("gak ngapa2in..");
						break;}
					case "1" :
					case "2" :
					case "3" :
					case "4" :
					case "5" :
					case "6" :
					case "7" :
					case "8" :
					case "9" : {
						lblNewLabel.setVisible(false); // mall
						lblNewLabel_1.setVisible(false); // university
						lblNewLabel_2.setVisible(false); // cafe
						lblNewLabel_3.setVisible(false); // prerequisite kado
						lblNewLabel_4.setVisible(true); // rumah cewe
						lblNewLabel_5.setVisible(false); // gym
						lblNewLabel_6.setVisible(false); // *ga ngapa2in*
						text.setText("kunjungin cewe ke "+solusi.substring(counter, counter+1));
						break;
					}
					case "m" :{
						lblNewLabel.setVisible(true); // mall
						lblNewLabel_1.setVisible(false); // university
						lblNewLabel_2.setVisible(false); // cafe
						lblNewLabel_3.setVisible(false); // prerequisite kado
						lblNewLabel_4.setVisible(false); // rumah cewe
						lblNewLabel_5.setVisible(false); // gym
						lblNewLabel_6.setVisible(false); // *ga ngapa2in*
						text.setText("Nanto pergi ke mall");
						break;}
					case "g" :{
						lblNewLabel.setVisible(false); // mall
						lblNewLabel_1.setVisible(false); // university
						lblNewLabel_2.setVisible(false); // cafe
						lblNewLabel_3.setVisible(false); // prerequisite kado
						lblNewLabel_4.setVisible(false); // rumah cewe
						lblNewLabel_5.setVisible(true); // gym
						lblNewLabel_6.setVisible(false); // *ga ngapa2in*
						text.setText("Nanto pergi ke gymnasium");
						break;}
					case "c" :{
						lblNewLabel.setVisible(false); // mall
						lblNewLabel_1.setVisible(false); // university
						lblNewLabel_2.setVisible(true); // cafe
						lblNewLabel_3.setVisible(false); // prerequisite kado
						lblNewLabel_4.setVisible(false); // rumah cewe
						lblNewLabel_5.setVisible(false); // gym
						lblNewLabel_6.setVisible(false); // *ga ngapa2in*
						text.setText("Nanto pergi ke cafe..");
						break;}
					case "u " :{
						lblNewLabel.setVisible(false); // mall
						lblNewLabel_1.setVisible(true); // university
						lblNewLabel_2.setVisible(false); // cafe
						lblNewLabel_3.setVisible(false); // prerequisite kado
						lblNewLabel_4.setVisible(false); // rumah cewe
						lblNewLabel_5.setVisible(false); // gym
						lblNewLabel_6.setVisible(false); // *ga ngapa2in*
						text.setText("Nanto pergi ke univ buat belajar");
						break;
					
					}
					case "A":
					case "B":
					case "C":
					case "D":
					case "E":
					case "F":
					case "G":
					case "H":
					case "I":
					case "J":
					case "K":
					case "L":
					case "M":
					case "N":
					case "O":
					case "P":
					case "Q":
					case "R":
					case "S":
					case "T":
					case "U":
					case "V":
					case "W":
					case "X":
					case "Y":
					case "Z":
					{
						lblNewLabel.setVisible(false); // mall
						lblNewLabel_1.setVisible(false); // university
						lblNewLabel_2.setVisible(false); // cafe
						lblNewLabel_3.setVisible(true); // prerequisite kado
						lblNewLabel_4.setVisible(false); // rumah cewe
						lblNewLabel_5.setVisible(false); // gym
						lblNewLabel_6.setVisible(false); // *ga ngapa2in*
						text.setText("beli kado yang berkode "+solusi.substring(counter, counter+1));
						break;
					}
					}}catch(Exception x){
						x.printStackTrace();
					}finally{
						counter++;	
					}
					try {
						Thread.sleep(500);
					}
					catch(Exception e) {
						
					}
				}
				
			}
			
		});
		myThread.start();
		
	}
	
}
