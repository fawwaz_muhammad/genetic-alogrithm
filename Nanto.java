import java.util.Random;

public class Nanto{
		public int currang;
		public int currwaktu;
		public int currenergi;
		public String curritem;
		public int currstrength;
		public int currcharm;
		public int currbrain;
		public char currplace;
		public int currenlightment;
		public String jadwalnanto;
		
		// Constructor
		public Nanto(){
			currang = ai.modalUang;
			currwaktu = ai.waktu;
			currenergi = ai.energi;
			curritem = ""; 
			currstrength = ai.strengthAwal;
			currcharm = ai.charmAwal;
			currbrain = ai.brainAwal;
			currplace = '0';
			currenlightment = 0;
		}
		
		public void generatejadwalnanto(){
			int panjang = ai.waktu * 7 * 12;
			String[] valids =  Myvalidator.ArrayValid;
			String output = "";
			Random randomizer = new Random();
			
			for (int i = 0; i < panjang; i++) {
				int j = randomizer.nextInt(valids[i].length());
				output += valids[i].charAt(j);
			}
			jadwalnanto = output;
		}
}		