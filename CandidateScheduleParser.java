import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class CandidateScheduleParser{
	static final int NumberOfHours = 7 * 12;
	static final int NumberOfCandidates = ai.NumberOfCandidate;
	
	//contains the schedule, where
	//the first bracket represents the candidate (1 to 4)
	//the second bracket represents the hour (10.00 to 21.00)
	public static int[][] schedule;
	
	//ctor
	public CandidateScheduleParser(){
		schedule = new int[NumberOfCandidates][NumberOfHours];
	}
	
	/*methods*/
	//read schedule from file to ScheduleArray
	public void ReadFile(){
		int rowcounter = 0;
		//read the text file
		String strschedule = "";
		String linereader;
		try{
		   BufferedReader buffreader = new BufferedReader(new FileReader("CandidateSchedule.txt"));
		   //read the text file
		   while((linereader = buffreader.readLine()) != null){
				strschedule = linereader;
				//store into ScheduleArray
				for(int j = 0; j < NumberOfHours; j++){
					if(strschedule.charAt(j) == '1'){
						schedule[rowcounter][j] = 1;
					}
				}
				//go to the next candidate's schedule
				rowcounter++;
		   }
		}
		catch(IOException e){
		   System.out.println("File not found");
		}
	}
	
	//print the schedule (for testing purposes)
	public void ShowSchedule(){
		System.out.println("Jadwal Kandidat dari Senin sampai Minggu");
		for(int i = 0; i < NumberOfCandidates; i++){
			System.out.print("Kandidat "+(i+1)+" ");
				for(int j = 0; j < NumberOfHours; j++){
					System.out.print(schedule[i][j]);
					if((j+1) % 7 == 0){
						System.out.print("|");
					}
				}
			System.out.println("");
		}
	}
	
	//check if a candidate is available at a time
	//candidateNumber ranges from 0 to 3
	//timeCode represents time in a week (0-83)
	public boolean isCandidateAvailable(int candidateNumber, int timeCode){
		if(schedule[candidateNumber][timeCode] == 1){
			return true;
		}
		else{
			return false;
		}
	}
	
	//public static void main(String args[]){
		//CandidateScheduleParser csp = new CandidateScheduleParser();
		//csp.ReadFile();
		//csp.ShowSchedule();
		//System.out.println("ahay ahay "+csp.isCandidateAvailable(1,0));
	//}
	
}
