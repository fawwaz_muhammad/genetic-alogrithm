// package
import java.util.*;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class Myvalidator {
	/*
	 * Array Valid : 
	 * Representasi Alfabet/Huruf Apa saja yang valid untuk waktu ke-"n"
	 * Karena tidak setiap tempat/kandidat bisa ditemui pada waktu ke-"n"
	 * ============
	 * MinimumPlaceVisit:
	 * Berapa lama nanto harus mengunjungi gym/cafe/university untuk mencapai attribut minimum
	 * MinimumPlaceVisit[KodeKandidat][Kodetempat]. Fungsi ini sudah memperhitungkan waktu libur
	 * tempat. (Dalam satuan waktu ke-"n")
	 * Kode tempat 1 = Gymnasium
	 * Kode tempat 2 = Cafe
	 * Kode tempat 3 = University
	 * ============
	 * MostMinimumTime:
	 * Nilai maksimum diantara MinimumPlaceVisit sehingga, Kandidat dapat ditemui
	 * Contoh : Kandidat 1 Setelah dilakukan kalkulasi MinimumPlace Visitnya adalah
	 * Gym 15 Kali, cafe 10 kali
	 * ============
	 * JumlahWaktu :
	 * Total periode kesempatan yang diberikan untuk nanto berkenalan. ex: 2 pekan = 2 pekan * 12 jam * 7 Hari 
	 * */
	public static String[] ArrayValid = new String[ai.waktu * 7 * 12 ];
	private static int[][] MinimumPlaceVisit = new int[ai.NumberOfCandidate][4];
	public static int[] MostMinimumTime = new int[ai.NumberOfCandidate];
	public static int jumlahwaktu = ai.waktu * 7 * 12;;
	
	
	public static final char HURUFGYM = 'g';	
	public static final char HURUFCAFE = 'c';
	public static final char HURUFUNIV = 'u';
	public static final char HURUFMALL = 'm';
	public static int enlightmentscore;
	// Constructor
	public void MyValidator(){
//		BuildValidArray();
//		BuildValidSkill();
	}
	
	
	/* *
	 * BuildValidArray
	 * */
	public void BuildValidArray(){
		String alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int mostmostminimum =0;
		// Jadwal Schedule
		// Inisialisasi awal sebagai empty string
		for (int i = 0; i < jumlahwaktu; i++) {
			//ArrayValid[i] = alfabet.substring(0,ai.arrayitem.size());
			ArrayValid[i] = "";
		}
		
		// diantara yang paling minimum, cari calon mana yang paling akhir,
		// setelah i mencapai syarat calon paling akhir, tidak perlu lagi ke gym / cafe /university
		for (int i = 0; i < ai.NumberOfCandidate; i++) {
			if(mostmostminimum<MostMinimumTime[i]){
				mostmostminimum = MostMinimumTime[i];
			}
		}
			
		
		for (int i = 0; i < jumlahwaktu; i++) {
			int j = i % (7*12);
			
			
			// Representasi dari Mall itu di index pertama array schedule
			if(PlaceScheduleParser.schedule[0][j]==1){
				ArrayValid[i] = ArrayValid[i] + "m";
			}
			
			// Representasi dari Gymnasium itu di index kedua array schedule
			if(PlaceScheduleParser.schedule[1][j]==1){
				ArrayValid[i] = ArrayValid[i] + "g";
			}
			// Representasi dari Cafe itu di index ketiga array shcedule
			if(PlaceScheduleParser.schedule[2][j]==1){
				ArrayValid[i] = ArrayValid[i] + "c";
			}
			// Representasi dari Jadwal University Yang alvailable ada di index keempat array schedule
			if(PlaceScheduleParser.schedule[3][j]==1){
				ArrayValid[i] = ArrayValid[i] + "u";
			}
			
			
			
			
			
			
			// Representasi dari Jadwal Candidate 
			for (int k = 0; k < ai.NumberOfCandidate; k++) {
				// Better check if it is calculated minimum start from
				
				// EDIT DISINI				
				// Menghitung seberapa cepat candidate tersebut dapat ditemui berasarkan
				// syarat minimal Charm / Brain / Strength
				Candidate C = ai.arraycandidate.get(k);
				ArrayList<Integer> MinAttr = new ArrayList<Integer>();
				
				// Menghitung berapa lama kandidat harus ke gym untuk mencapai 
				// Strength Minimum yang dibutuhkan untuk menemui Kandidat ke - i
				MinAttr.add(CountMinTimeForStrength(C.StrengthNeeded));
				
				// Menghitung berapa lama kandidat harus ke cafe untuk mencapai 
				// Charm Minimum yang dibutuhkan untuk menemui Kandidat ke - i
				MinAttr.add(CountMinTimeForCharm(C.CharmNeeded));
						
				// Menghitung berapa lama kandidat harus ke university untuk mencapai 
				// Body Minimum yang dibutuhkan untuk menemui Kandidat ke - i
				MinAttr.add(CountMinTimeForBrain(C.BrainNeeded));
				
				MinimumPlaceVisit[k][Place.KODEGYM]  = MinAttr.get(0);   
				MinimumPlaceVisit[k][Place.KODECAFE] = MinAttr.get(1);
				MinimumPlaceVisit[k][Place.KODEUNIV] = MinAttr.get(2);
						
				// Menyimpan mostminiimum time
				MostMinimumTime[k] = Collections.max(MinAttr)+CountMinTimeForItem(k);
				
				//System.out.println("waktu ke -"+i+" kandidat ke = " + Integer.toString(k) + "Most minimumnya"+MostMinimumTime[k]);
				if(i>MostMinimumTime[k]){
					if(CandidateScheduleParser.schedule[k][j]==1){
						// ditambah satu karena index milik calon dimulai dari 1
						ArrayValid[i] = ArrayValid[i] + Integer.toString(k+1);
					}
				}else{
					// kalau belum sempet masuk SCB + waktu untuk beli item
					// Tambahin Array validnya degnan item yang dibutuhkan oleh kandidat
					ArrayValid[i] += ai.arraycandidate.get(k).Prerequisite; 
				}
			}
			//System.out.print("Array valid ke - "+i+" : "+ ArrayValid[i]+"\n");			
		}
		
		for (int k = 0; k < ai.NumberOfCandidate; k++) {
			//System.out.println("Kandidat ke -" +k+ MostMinimumTime[k]);
		}
		
	}

	
	private int CountMinTimeForItem(int urutankandidat){
		return ((ai.waktu * 7 * 12) - MostMinimumTime[urutankandidat])/ai.arraycandidate.get(urutankandidat).Max ;
	}
	
	private int CountMinTimeForStrength(int strengthneeded){
		int counter = 0, i, j=0, mincounter = 0;
		Place gym = ai.arrayplace.get(Place.KODEGYM);
		mincounter = (int) Math.ceil(strengthneeded/gym.AttributeAdded);
		for (i = 0; i < jumlahwaktu; i++) {
			if(ArrayValid[i].indexOf(HURUFGYM)!= -1){
				counter++;
			}
			j++;
			if(mincounter==counter){
				break;
			}
		}
		return j;
	}
	
	private int CountMinTimeForCharm(int charmneeded){
		int counter = 0, i,j=0, mincounter = 0;
		Place cafe = ai.arrayplace.get(Place.KODECAFE);
		mincounter = (int) Math.ceil(charmneeded/cafe.AttributeAdded);
		for (i = 0; i < jumlahwaktu; i++) {
			if(ArrayValid[i].indexOf(HURUFCAFE)!= -1){
				counter++;
			}
			j++;
			if(mincounter==counter){
				break;
			}
		}
		return j;
	}

	private int CountMinTimeForBrain(int brainneeded){
		int counter = 0, i,j= 0 , mincounter = 0;
		Place univ = ai.arrayplace.get(Place.KODEUNIV);
		mincounter = (int) Math.ceil(brainneeded/univ.AttributeAdded);
		for (i = 0; i < jumlahwaktu; i++) {
			if(ArrayValid[i].indexOf(HURUFUNIV)!= -1){
				counter++;
			}
			j++;
			if(mincounter==counter){
				break;
			}
		}
		return j;
	}

	
	

	public ArrayList<Integer> CekAlvailable(String output){
		ArrayList<Integer> fault = new ArrayList<Integer>();
		int b=0;
		for (int i = 0; i < (ai.waktu * 12 * 7 ); i++) {
			char c = output.charAt(i);
			b++;
			if(ArrayValid[i].indexOf(c)==-1){ 
				// kalau c tidak ditemukan di dalam array valid
				fault.add(b);
			}
		}
		return fault;
	}
	
	
	public ArrayList<Integer> Check(String output){
		int b=0;
		Nanto N = new Nanto(ai.modalUang, 0, ai.energi, "", ai.strengthAwal, ai.charmAwal, ai.brainAwal);
		ArrayList<Integer> Fault =new ArrayList<>();
		//System.out.println(output);
//		if(Fault.size()>0){
//			System.out.println("IDIOT");
//			return Fault;
//		}
		for (int i = 0; i < (ai.waktu * 7) ; i++) {// hari
			// Setiap hari reset energi dan jumlah pertemuan dengan kandidat
			// dan reset juga jumlah buy daily
			N.currentenergi = ai.energi;
			for (int j = 0; j < ai.NumberOfCandidate; j++) {
				N.currentmeetcounter[j]= 0;
			}
			
			for (int j = 0; j < 12; j++) { // jam
				int k = i * 12 + j;
				char c = output.charAt(k);
				//System.out.print(c +" ");
				switch (c) {
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						// Artnya kenalan sama kandidat
						// Cek syarat kenalan sama kandidat
						int a = Character.getNumericValue(c)-1;
						//System.out.println("Kandidat nomor : "+c + " PREREQ " +ai.arraycandidate.get(a).Prerequisite + "Nanto punya ? "+IsNantoHaveItem(N.currentitem, ai.arraycandidate.get(a).Prerequisite));
						
//						System.out.println("Nomor Kandidat"+a);
//						System.out.print("\tS:"+(N.currentbrain < ai.arraycandidate.get(a).StrengthNeeded));
//						System.out.print("\tC:"+(N.currentbrain < ai.arraycandidate.get(a).CharmNeeded));
//						System.out.print("\tB:"+(N.currentbrain < ai.arraycandidate.get(a).BrainNeeded));
//						System.out.print("\nSyrat item :"+ IsNantoHaveItem(N.currentitem, ai.arraycandidate.get(a).Prerequisite));
//						System.out.print("\nSyrat Currentmeetcounter "+((N.currentmeetcounter[a]) >= ai.arraycandidate.get(a).Max) + "yaitu " + N.currentmeetcounter[a]);
//						System.out.println("\n###Posisi Waktu ke :"+k+"\n");
//						printNanto(N);
						/*if (N.currentbrain<ai.arraycandidate.get(a).BrainNeeded){
							System.out.println(k + " " + "Brain error");
						}
						if (N.currentstrength<ai.arraycandidate.get(a).StrengthNeeded){
							System.out.println(k + " " + "Strength error");
						}
						if (N.currentcharm<ai.arraycandidate.get(a).CharmNeeded){
							System.out.println(k + " " + "Charm error");
						}
						if (N.currentenergi<ai.arraycandidate.get(a).EnergiperHours){
							System.out.println(k + " " + "Energy error");
						}
						if (!IsNantoHaveItem(N.currentitem, ai.arraycandidate.get(a).Prerequisite)){
							System.out.println("Item error");
						}
						if (N.currentmeetcounter[a] >= ai.arraycandidate.get(a).Max){
							System.out.println("Meet maximal error");
						}*/
						
						if(
							(N.currentbrain < ai.arraycandidate.get(a).BrainNeeded) 
							|| (N.currentstrength < ai.arraycandidate.get(a).StrengthNeeded) 
							|| (N.currentcharm < ai.arraycandidate.get(a).CharmNeeded)
							|| (N.currentenergi < ai.arraycandidate.get(a).EnergiperHours
							|| !IsNantoHaveItem(N.currentitem, ai.arraycandidate.get(a).Prerequisite))
							|| (N.currentmeetcounter[a]) >= ai.arraycandidate.get(a).Max
						){
							//System.out.println("fault0");
							Fault.add(b);
						}else{
							// berhasil kenalan
							N.currentitem.replaceFirst(ai.arraycandidate.get(a).Prerequisite, "");
							N.currentenergi -= ai.arraycandidate.get(a).EnergiperHours;
							N.currentenlightment += ai.arraycandidate.get(a).EnlightmentAddition;
							N.currentmeetcounter[a]+=1;
							enlightmentscore = N.currentenlightment;
						}
						break;
					case 'm':
						if (N.currentenergi < ai.arrayplace.get(Place.KODEMALL).EnergiNeeded){
							//System.out.println("fault1");
							Fault.add(b);
						}else
						{
							// berhasil ke mall
							N.currentuang += ai.arrayplace.get(Place.KODEMALL).AttributeAdded;
							N.currentenergi -= ai.arrayplace.get(Place.KODEMALL).EnergiNeeded;
						}
						break;
					case 'g':
						if (N.currentenergi < ai.arrayplace.get(Place.KODEGYM).EnergiNeeded){
							//System.out.println("fault2");
							Fault.add(b);
						}else
						{
							// berhasil ke mall
							N.currentstrength += ai.arrayplace.get(Place.KODEGYM).AttributeAdded;
							N.currentenergi -= ai.arrayplace.get(Place.KODEGYM).EnergiNeeded;
						}
						break;
					case 'c':
						if (N.currentenergi < ai.arrayplace.get(Place.KODECAFE).EnergiNeeded){
							//System.out.println("fault3");
							Fault.add(b);
						}else
						{
							// berhasil ke mall
							N.currentcharm += ai.arrayplace.get(Place.KODECAFE).AttributeAdded;
							N.currentenergi -= ai.arrayplace.get(Place.KODECAFE).EnergiNeeded;
						}
						break;
					case 'u':
						if (N.currentenergi < ai.arrayplace.get(Place.KODEUNIV).EnergiNeeded){
							//System.out.println("fault4");
							Fault.add(b);
						}else
						{
							// berhasil ke mall
							N.currentbrain += ai.arrayplace.get(Place.KODEUNIV).AttributeAdded;
							N.currentenergi -= ai.arrayplace.get(Place.KODEUNIV).EnergiNeeded;
						}
						break;
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					case 'M':
					case 'N':
					case 'O':
					case 'P':
					case 'Q':
					case 'R':
					case 'S':
					case 'T':
					case 'U':
					case 'V':
					case 'W':
					case 'X':
					case 'Y':
					case 'Z':
						// artinya beli item 
						// diasumsikan tidak ada syarat energi
						if(
							(N.currentuang < ai.arrayitem.get(c).Harga)
							|| (N.currentdailybuy[AlfabetToInt(c)] >= ai.arrayitem.get(c).Harga)
						){
							//System.out.println("fault5");
							Fault.add(b);
						}else{
							N.currentuang -= ai.arrayitem.get(c).Harga;
							N.currentitem += c;
							N.currentdailybuy[AlfabetToInt(c)] += 1;
						}
						break;
					case '0':
					default:
						N.currentenergi -=0;
						break;
				}
				b++;
				// Melakukan "push" jika lebih dalam sehari
//				if(N.currentenergi<0){
//					Integer z = b;
//					z = z-1;
//					Fault.add(z);
//				}
				//System.out.println("Jam ke: "+k+ " - Energi: "+ N.currentenergi);
			}
		}
		//enlightmentscore = N.currentenlightment;
		/*System.out.println(Fault.size());
		for (int i=0;i<Fault.size();i++){
			System.out.print(Fault.get(i)+ " ");
		}
		System.out.println();*/
		//Fault = new ArrayList<Integer>();
		return Fault;
	}
	
	public Boolean IsNantoHaveItem(String item_nanto,String Prerequisite){
		//System.out.println("Prereq length "+Prerequisite+" " + Prerequisite.length());
		if(Prerequisite.length()==0){
			return true;
		}
		//System.out.println("Prerequisite ke -"+i+" adalah "+c+" dan item yang dimiliki nanto + "+item_nanto+"Apakah nanto punya ");
		for (int i = 0; i < Prerequisite.length(); i++) {
			char c = Prerequisite.charAt(i);
			//System.out.print("Huruf "+c+" ditemukan di "+item_nanto.indexOf(c));
			if(item_nanto.indexOf(c)==-1){
				return false;
			}
		}
		return true;
	}
	
	private int AlfabetToInt(char c){
		return c-'A';
	}
	
	public static class Nanto{
		public int currentuang;
		public int currentwaktu;
		public int currentenergi;
		public String currentitem;
		public int currentstrength;
		public int currentcharm;
		public int currentbrain;
		public char currentplace;
		public int[] currentmeetcounter = new int[ai.NumberOfCandidate];
		public int[] currentdailybuy = new int[ai.NumberOfItems];
		public int currentenlightment;
		public Nanto(int uang,int waktu,int energi,String item,int strength,int charm,int brain){
			currentenlightment = 0;
			currentuang     =  uang;
			currentwaktu    =  waktu;
			currentenergi   =  energi;
			currentitem     =  item;
			currentstrength =  strength;
			currentcharm    =  charm;
			currentbrain    =  brain;
			for (int i = 0; i < ai.NumberOfCandidate; i++) {
				currentmeetcounter[i] = 0;
			}
			for (int i = 0; i < ai.NumberOfItems; i++) {
				currentdailybuy[i] = 0;
			}
		}
	}
	public int countenlight(String s){
		int totalenlight=0;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			switch (c) {
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				totalenlight+=ai.arraycandidate.get(Character.getNumericValue(c)-1).EnlightmentAddition;
				break;
			default:
				break;
			}
		}
		return totalenlight;
	}
	
	public void printNanto(Nanto N){
		System.out.print("NANTO\n=====");
		System.out.print("\nEnergi:"+N.currentenergi);
		System.out.print("\tS:"+N.currentstrength);
		System.out.print("\tC:"+N.currentcharm);
		System.out.print("\tB:"+N.currentbrain);
		System.out.print("\nitem:"+N.currentitem);
		System.out.print("\nMeet Counter Kandidat");
		for (int i = 0; i < ai.NumberOfCandidate; i++) {
			System.out.print("\t["+i+"]:"+N.currentmeetcounter[i]);
		}
		System.out.print("\nCounter Item");
		for (int i = 0; i < ai.NumberOfItems; i++) {
			System.out.print("\t["+i+"]"+N.currentdailybuy[i]);
		}
		
		
	}
	
	public static String GeraldiGenerator(){
		Place mall = ai.arrayplace.get(Place.KODEMALL);
		char[] str = new char[jumlahwaktu];
		for (int i=0;i<jumlahwaktu;i++){
			str[i]='0';
		}
		int uang = ai.modalUang;
		int energi = ai.energi;
		// harian
		//String str = "";
		Nanto n = new Nanto(ai.modalUang, 0, ai.energi, "", ai.strengthAwal, ai.charmAwal, ai.brainAwal);
		ArrayList<Integer> Minattr = new ArrayList<>();
		
		Place gym = ai.arrayplace.get(Place.KODEGYM);
		Place cafe = ai.arrayplace.get(Place.KODECAFE);
		Place univ = ai.arrayplace.get(Place.KODEUNIV);
		
		for (int i = 0; i < ai.NumberOfCandidate; i++) {
			Candidate c = ai.arraycandidate.get(i);
			
			MinimumPlaceVisit[i][1] = (int)Math.ceil(c.StrengthNeeded / gym.AttributeAdded); 
			MinimumPlaceVisit[i][2] = (int)Math.ceil(c.CharmNeeded / cafe.AttributeAdded);
			MinimumPlaceVisit[i][3] = (int)Math.ceil(c.BrainNeeded / univ.AttributeAdded);
		}
		int maxGym=0;
		int maxCafe=0;
		int maxUniv=0;
		for (int i=0;i<ai.NumberOfCandidate;i++){
			if (MinimumPlaceVisit[i][1] > maxGym){
				maxGym = MinimumPlaceVisit[i][1];
			}
			if (MinimumPlaceVisit[i][2] > maxCafe){
				maxCafe = MinimumPlaceVisit[i][2];
			}
			if (MinimumPlaceVisit[i][3] > maxUniv){
				maxUniv = MinimumPlaceVisit[i][3];
			}
		}
		int tt,t1,t2,t3;
		int i = 0;
		int j = 0;
		int iGym=1, iCafe=1, iUniv=1;
		int tim = 0;
		int counter[] = new int[100];
		for (i=0;i<100;i++){
			counter[i] =0;
		}
		Random randomizer = new Random();
		while ((iGym < maxGym || iCafe < maxCafe || iUniv < maxUniv) && (tim < jumlahwaktu)){
			
			if (tim%12 == 0 && tim != 0)
				energi = ai.energi;
			if (iGym < maxGym && PlaceScheduleParser.schedule[1][tim%84] == 1 && str[tim] == '0' && energi-gym.EnergiNeeded >= 0){
				str[tim]='g';
				energi-=gym.EnergiNeeded;
				tim++;
				iGym++;
				continue;
			}
			else if (iCafe < maxCafe && PlaceScheduleParser.schedule[2][tim%84] == 1 && str[tim] == '0'&& energi-cafe.EnergiNeeded >= 0){
				str[tim]='c';
				tim++;
				energi-=cafe.EnergiNeeded;
				iCafe++;
				continue;
			}
			else if (iUniv < maxUniv && PlaceScheduleParser.schedule[3][tim%84] == 1 && str[tim] == '0'&& energi-univ.EnergiNeeded >= 0){
				str[tim]='u';
				tim++;
				energi-=univ.EnergiNeeded;
				iUniv++;
				continue;
			}
			else {
				
				int x = randomizer.nextInt(ai.NumberOfCandidate);
				boolean valid = true;
				Candidate can = ai.arraycandidate.get(x);
				int counter2[] = new int[100];
				for (j=0;j<100;j++){
					counter2[j] =0;
				}
				for (j=0;j<can.Prerequisite.length();j++){
					counter2[can.Prerequisite.charAt(j)]++;
				}
				for (j='A';j<='Z';j++){
					if (counter2[j] > counter[j]){
						valid = false;
					}
				}
				if (iUniv*univ.AttributeAdded < ai.arraycandidate.get(x).BrainNeeded)
					valid = false;
				if (iGym*gym.AttributeAdded < ai.arraycandidate.get(x).StrengthNeeded)
					valid = false;
				if (iCafe*cafe.AttributeAdded < ai.arraycandidate.get(x).CharmNeeded)
					valid = false;
				if (energi-ai.arraycandidate.get(x).EnergiperHours < 0)
					valid = false;
				if (CandidateScheduleParser.schedule[x][i%84] == 0)
					valid = false;
				
				if (!valid){
					int id_barang = randomizer.nextInt(ai.NumberOfItems);
					id_barang += 'A';
					counter[id_barang]++;
					if (uang-ai.arrayitem.get((char) id_barang).Harga >= 0){
						str[tim] = (char) id_barang;
						uang-=ai.arrayitem.get((char) id_barang).Harga;
					}
					else {
						if (str[tim] == '0' && PlaceScheduleParser.schedule[0][tim%84] == 1 && energi-mall.EnergiNeeded >= 0){
							str[tim] = 'm';
							energi-=mall.EnergiNeeded;
							uang += mall.AttributeAdded;
						}
						else {
							if (str[tim] != '0'){
								//System.out.println("IMPOSSIBLE");
							}
							else if (PlaceScheduleParser.schedule[0][tim%84] == 0){
								//System.out.println("TUTUP");
							}
							else if (energi-mall.EnergiNeeded < 0){
								//System.out.println("no energy");
							}
							
							// Do NOTHING
						}
					}
					
				}
				else {
					energi -= ai.arraycandidate.get(x).EnergiperHours;
					str[tim] = (char) ('1' + x);
					for(j='A';j<='Z';j++){
						counter[j]-=counter2[j];
					}
				}
				tim++;
			}
		}
		//System.out.println("ANJINGGGGGGGGGGGGGGGGG " + maxGym + " " + maxCafe+ " "+maxUniv);
		
		tt = tim;
		//System.out.println("TRESHOLD " + tt);
		
		
		for (i = tt;  i < jumlahwaktu; i++) {
			if (tt%12 == 0)
				energi = ai.energi;
			int x = randomizer.nextInt(ai.NumberOfCandidate);
			boolean valid = true;
			Candidate can = ai.arraycandidate.get(x);
			int counter2[] = new int[100];
			for (j=0;j<100;j++){
				counter2[j] =0;
			}
			for (j=0;j<can.Prerequisite.length();j++){
				counter2[can.Prerequisite.charAt(j)]++;
			}
			for (j='A';j<='Z';j++){
				if (counter2[j] > counter[j]){
					valid = false;
				}
			}
			if (energi-ai.arraycandidate.get(x).EnergiperHours < 0)
				valid = false;
			if (CandidateScheduleParser.schedule[x][i%84] == 0)
				valid = false;
			if (!valid){
				int id_barang = randomizer.nextInt(ai.NumberOfItems);
				id_barang += 'A';
				counter[id_barang]++;
				if (uang-ai.arrayitem.get((char) id_barang).Harga >= 0){
					str[i] = (char) id_barang;
					uang-=ai.arrayitem.get((char) id_barang).Harga;
				}
				
			}
			else {
				//System.out.println("COI MASUK ANJIRRR");
				energi -= ai.arraycandidate.get(x).EnergiperHours;
				str[i] = (char) ('1' + x);
				for(j='A';j<='Z';j++){
					counter[j]-=counter2[j];
				}
			}
		}
		String strstr = "";
		for (i=0;i<jumlahwaktu;i++){
			 strstr += str[i];
		}
		return strstr;
		/*
		Random randomizer = new Random();
		for (i=tt;i<jumlahwaktu;i++){
			for (j=0;j<ai.NumberOfCandidate;j++){
			
			int x= randomizer.nextInt(ai.NumberOfCandidate);
			Candidate can = ai.arraycandidate.get(x);
			if (){
				
			}
		}*/
		
		// Komparasi antara ke tiga kandidat berapa kali minimal dia apa??
		/*
		for (int i = 0; i < 7; i++) {
			
			// Reset energi
			n.currentenergi = ai.energi;
			for (int j = 0; j < 12; j++) {
				int k = i*12 + j;
				
			}
		}*/
	}
	
	//public ArrayList<Integer> ValidasiEnergiPerHari(String output){
//		int counterenergi=0,b=0,energi = ai.energi;
//		ArrayList<Integer> Fault = new ArrayList<Integer>();
//		for (int i = 0; i < (ai.waktu * 7) ; i++) {// hari
//			counterenergi =0;
//			for (int j = 0; j < 12; j++) { // jam
//				int k = i * 12 + j;
//				char c = output.charAt(k);
//				switch (c) {
//					case '1':
//					case '2':
//					case '3':
//					case '4':
//					case '5':
//					case '6':
//					case '7':
//					case '8':
//					case '9':
//						// Artnya kenalan sama kandidat
//						counterenergi += ai.arraycandidate.get(a).EnergiperHours;
//						break;
//					case 'm':
//						counterenergi += ai.arrayplace.get(Place.KODEMALL).EnergiNeeded; 
//						break;
//					case 'g':
//						counterenergi += ai.arrayplace.get(Place.KODEGYM).EnergiNeeded;
//						break;
//					case 'c':
//						counterenergi += ai.arrayplace.get(Place.KODECAFE).EnergiNeeded;
//						break;
//					case 'u':
//						counterenergi += ai.arrayplace.get(Place.KODEUNIV).EnergiNeeded;
//						break;
//					case '0':
//					default:
//						counterenergi +=0;
//						break;
//				}
//				b++;
//				// Melakukan "push" jika lebih dalam sehari
//				if(counterenergi>energi){
//					Integer z = b;
//					z = z-1;
//					Fault.add(z);
//				}
//				System.out.println("Jam ke: "+k+ " - Energi: "+ counterenergi);
//			}
//		}
//		return Fault;
//	}
	
}