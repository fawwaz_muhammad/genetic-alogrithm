
public class Candidate {
	public Integer EnlightmentAddition;
	public Integer EnergiperHours;
	public Integer Max;
	public String Prerequisite;
	public Integer StrengthNeeded;
	public Integer CharmNeeded;
	public Integer BrainNeeded;

	public Candidate(Integer enlightment,Integer energiperhours, Integer max , String prerequisite, Integer strengthneeded, Integer charmneeded ,Integer brainneeded){
		this.EnlightmentAddition = enlightment;
		this.EnergiperHours      = energiperhours;
		this.CharmNeeded         = charmneeded;
		this.StrengthNeeded      = strengthneeded;
		this.BrainNeeded         = brainneeded;
		this.Prerequisite        = prerequisite;		
		this.Max 				 = max;
	}
}
