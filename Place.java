
public class Place {
	public static final int KODEMALL = 0;
	public static final int KODEGYM = 1;
	public static final int KODECAFE = 2;
	public static final int KODEUNIV = 3;
	
	public static final int KODEMONEY = 0;
	public static final int KODESTRENGTH = 1;
	public static final int KODECHARM = 2;
	public static final int KODEBRAIN = 3;
	
	public Integer EnergiNeeded;
	public Integer AttributeName;
	public Integer AttributeAdded;

	public Place(int energineeded,int attributeadded,int attributename){
		this.EnergiNeeded   = energineeded;
		this.AttributeName  = attributename;
		this.AttributeAdded = attributeadded;
	}
}
