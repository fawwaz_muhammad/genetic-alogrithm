import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
//import javax.xml.bind.Validator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;


public class ai extends JFrame {

	private JPanel contentPane;

	private Nanto N;

	// Seluruh variabel dibawah adalah variabel yang dianggap global 
	// hasil pembacaan dari file eksternal
	// perhatikan terdapat arrayitem, bedakan array item ini dengan array item yang 
	// terdapat di kelas Nanto.java (Array item yang dipegang oleh nanto pada jam ke-"n").
	static ArrayList<Candidate> arraycandidate;
	static Integer NumberOfCandidate;
	static Map<Character,Items> arrayitem;
	static Integer NumberOfItems;
	static Integer modalUang, waktu, energi, strengthAwal, charmAwal, brainAwal;
	static ArrayList<Place> arrayplace;
	static Integer[][] PlaceSchedule;
	static Integer[][] CandidateSchedule;
	 //String res;
	public GUI g;
	// Deklarasi seluruh parser
	private GeneralInformationParser GIP;
	private CandidateScheduleParser CSP;
	private PlaceScheduleParser PSP;
	
	void InitVariables(){
		arraycandidate = new ArrayList<>();
		arrayitem = new HashMap<>();
		
		// Karena tidak disebutkan dalam soal Attribut place(Energi Needed dst..) 
		// dapat dikonfigurasi melalui file eksternal maka di serluuhnya dihardcode disini
		Place mall = new Place(8,100000,Place.KODEMONEY);
		Place gym = new Place(12,2,Place.KODESTRENGTH);
		Place cafe = new Place(6,2,Place.KODECHARM);
		Place univ = new Place(15,3,Place.KODEBRAIN);
		
		arrayplace = new ArrayList<>();
		arrayplace.add(mall);
		arrayplace.add(gym);
		arrayplace.add(cafe);
		arrayplace.add(univ);
		
		GIP = new GeneralInformationParser();
		GIP.ReadFile();
		
		CSP = new CandidateScheduleParser();
		CSP.ReadFile();
		
		PSP = new PlaceScheduleParser();
		PSP.ReadFile();
		
		Myvalidator V = new Myvalidator();		
		V.BuildValidArray();
		String calonsolusi = "c3g2gu0m1uum0uc1u231mc2ucmmm20g3uu1g3gmmg2g0c313331uuu310um1mmgcg1mgcu02g1g3mcu233mcgmmg1c3u130302113m1uug2g030g032c3gu0203u23213m20g0cg3c3g3u1u3u21g0g10gug2212020g1cm1";
		//String calonsolusi = "1g1CCAygD2AD2AcyB2cgmCyuEuAyguA2EmDA123B02AAu0cE0By03A0uB02ACcm1C0EDEgABA0ygcm1C2uCmmcymcCAAuCB2gmCC2c02BcycD1gyBA0mDy0BucAAD2C31ACgg0AA10uEuuAEuD01E1C23ymuuADE2BCAAEA2";
		ArrayList<Integer> failenergi = V.Check(calonsolusi);
		if(failenergi.size()>0){
//			System.out.print("Ada beberapa kesalahan yaitu : \n");
//			for (int i = 0; i < failenergi.size(); i++) {
//				System.out.print(failenergi.get(i)+ " ");
//			}
			//System.out.print("ada error");
		}else{
			
		}
		
	}
	
	
	
	/**
	 * Create the frame.
	 */
	public ai(GUI a) {
		InitVariables();
		
		g =a;
		
		// Testing apakah bisa keluar textnya
		String outputlabel = new String("<html>");
		for (int i = 0; i < NumberOfCandidate; i++) {
			Candidate C = arraycandidate.get(i);
			outputlabel = outputlabel+ "<br><<<Kandidat ke-" + i + ">>>";  
			outputlabel = outputlabel + "<br>EnlightmentAddition :" + C.EnlightmentAddition;
			outputlabel = outputlabel + "<br>EnergiperHours :" + C.EnergiperHours;
			outputlabel = outputlabel + "<br>CharmNeeded :" + C.CharmNeeded;
			outputlabel = outputlabel + "<br>StrengthNeeded :" + C.StrengthNeeded;
			outputlabel = outputlabel + "<br>BrainNeeded :" + C.BrainNeeded;
			outputlabel = outputlabel + "<br>Prerequisite :" + C.Prerequisite;
			outputlabel = outputlabel + "<br>Max :" + C.Max;
		}

		for (int i = 0; i < NumberOfItems; i++) {
			Character c = new Character('A');
			Items I = arrayitem.get(c);
			outputlabel = outputlabel+ "<br>=====";
			outputlabel = outputlabel + "<br>Harga :" + I.Harga;
			outputlabel = outputlabel + "<br>Restock :" + I.Restock;
		}
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMode = new JMenu("Mode");
		menuBar.add(mnMode);
		
		JMenuItem mntmSimulasiGa = new JMenuItem("Simulasi GA");
		mnMode.add(mntmSimulasiGa);
		
		JMenuItem mntmVisualisasi = new JMenuItem("Visualisasi ");

		mnMode.add(mntmVisualisasi);
		/*mntmVisualisasi.addActionListener(new ActionListener() {
			public void mouseClicked(MouseEvent arg0) {

				//System.out.print("Sekarang tinggal masukin string panel jpanel yang layout ");
				g.setVisible(true);
			}

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				

				System.out.print("Sekarang tinggal masukin string panel jpanel yang layout ");

			}
		});
		mnMode.add(mntmVisualisasi);*/
		
		JMenu mnAbout = new JMenu("About");
		menuBar.add(mnAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		//ImagePanel x = new ImagePanel(new ImageIcon("SIAPA_TAU_KEPAKE.png").getImage());
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		 
		JLabel lblStatus = new JLabel("Status ");
		panel.add(lblStatus, "4, 4, right, default");
		
		txtMutasiCross = new JTextField();
		txtMutasiCross.setText("Mutasi / Cross Over / Inisiasi Awal");
		panel.add(txtMutasiCross, "6, 4, fill, default");
		txtMutasiCross.setColumns(10);
		
		JLabel lblJumlahPopulasi = new JLabel("Jumlah Populasi");
		panel.add(lblJumlahPopulasi, "4, 6, right, default");
		
		textField = new JTextField();
		textField.setText("500");
		panel.add(textField, "6, 6, fill, default");
		textField.setColumns(10);
		
		JLabel lblHighestEnlightment = new JLabel("Highest Enlightment");
		panel.add(lblHighestEnlightment, "4, 8, right, default");
		
		textField_1 = new JTextField();
		textField_1.setText("290");
		panel.add(textField_1, "6, 8, fill, default");
		textField_1.setColumns(10);
		
		JLabel lblLowestEnlightment = new JLabel("Lowest Enlightment");
		panel.add(lblLowestEnlightment, "4, 10, right, default");
		
		textField_2 = new JTextField();
		textField_2.setText("190");
		panel.add(textField_2, "6, 10, fill, default");
		textField_2.setColumns(10);
		
		JLabel lblPopulasiSaatIni = new JLabel("Populasi saat ini");
		panel.add(lblPopulasiSaatIni, "4, 12");
		
		JTextArea txtrStringPopulasiSebagian = new JTextArea();
		txtrStringPopulasiSebagian.setText("String populasi Sebagian ditaruh disini kalau mau semuanya juga boleh");
		panel.add(txtrStringPopulasiSebagian, "6, 12, fill, fill");
		
		JButton btnStartSimulation = new JButton("Start Simulation");
		panel.add(btnStartSimulation, "6, 14");
		
		//JLabel lblTest = new JLabel(outputlabel);
		//contentPane.add(lblTest);
	}

	public static Character ConvertNumberToChar(int i){
		String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		return s.charAt(i);
	}
	public static Integer ConvertCharToNumber(char s){
		Integer i = Character.toUpperCase(s)- 'A' + 1;
		return i;
	}

	
	
	public static List<String> schedule;
	public static List<String> crossResult;
	public static int threshold;
	private JTextField txtMutasiCross;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	
	//CrossOver
	public static void crossOver(){	
		crossResult = new ArrayList<String>();
		for (int i=0;i<schedule.size();i++){
			Random rn = new Random();
			int maximum = schedule.size()-1;
			int minimum = 1;
			int range = maximum - minimum + 1;
			int firstrandomSol =  rn.nextInt(range) + minimum;
			int secondrandomSol =  rn.nextInt(range) + minimum;
			
			int rdm = rn.nextInt(schedule.get(0).length()-1) + minimum;
			//System.out.println("random length : "+rdm);
			//String 1
			String s1 = schedule.get(firstrandomSol).substring(rdm);
			String s12 = schedule.get(firstrandomSol).substring(0,rdm);
			//System.out.println("String 1 : " + schedule.get(firstrandomSol));
			
			//String 2
			String s2 = schedule.get(secondrandomSol).substring(rdm);
			String s21 = schedule.get(secondrandomSol).substring(0,rdm);
			//System.out.println("String 2 : " + schedule.get(secondrandomSol));
			
			//Cross
			String a = s12.concat(s2);
			String b = s21.concat(s1);
			
			crossResult.add(a);
			crossResult.add(b);
			
			//System.out.println("\nSchedule a : " + a);
			//System.out.println("Schedule b : " + b + "\n");
		}
	}
	
	
	//tukar karakter yang ada di dalam str
	public static String swap(String str, int idx1, int idx2){
		char[] chararray_str = str.toCharArray();
		
		char temp = chararray_str[idx1];
		chararray_str[idx1] = chararray_str[idx2];
		chararray_str[idx2] = temp;
		String result = new String(chararray_str);
		return result;
	}
	
	//Mutation
	public static void mutation(Myvalidator myvalidator){
		Random rnd = new Random();
		int maximum = crossResult.get(0).length()-1; //indeks maksimum mutasi
		int minimum = threshold; //indeks minimum mutasi
		int range = maximum - minimum + 1;
		//mengecek string hasil crossOver
		for(int i=0; i<crossResult.size(); i++){
			List<Integer> fault = myvalidator.Check(crossResult.get(i));
			
			//jika string hasil crossover sudah valid,
			//langsung masukkan ke List<String> schedule
			if(fault.size() == 0){
				//System.out.println("Waw, tidak perlu mutasi!");
				schedule.add(crossResult.get(i));
			}
			else{
				//jika string invalid, lakukan mutasi
				
				String invalid_string = crossResult.get(i);
				//System.out.println("string sebelum mutasi: "+invalid_string);
				//lakukan mutasi sebanyak (range/2) kali (penentuan banyak mutasi masih coba-coba)
				for(int j=0; j<(int)(range/2); j++){
					int firstmutationindex =  rnd.nextInt(range) + minimum;
					int secondmutationindex =  rnd.nextInt(range) + minimum;
					invalid_string = ai.swap(invalid_string,firstmutationindex,secondmutationindex);	
				}
				//System.out.println("string setelah mutasi: "+invalid_string);
				//cek lagi, apakah setelah mutasi string sudah dalam keadaan valid
				fault = myvalidator.Check(invalid_string);
				//jika ternyata valid, masukkan ke schedule
				if(fault.size() == 0){
					//System.out.println("Mutasi membuat string menjadi valid");
					schedule.add(invalid_string);
				}
			}
		}
	}
	
	public static void elimination(Myvalidator validator){
		List<Integer> values = new ArrayList<Integer>();
		List<Integer> values2 = new ArrayList<Integer>();
		List<String> newSchedule = new ArrayList<String>();
		List<String> newSchedule2 = new ArrayList<String>();
		int total = 0;
		for (int i=0;i<schedule.size();i++){
			values.add(validator.countenlight(schedule.get(i)));
			total += values.get(i);
			values2.add(validator.countenlight(schedule.get(i)));
		}
		Collections.sort(values);
		int rata2 = total/schedule.size();
		int tresshold = values.get(50*schedule.size()/100);
		//System.out.println(tresshold);
		//System.out.println("before " + schedule.size());
		String tmp = "";
		for (int i=0;i<schedule.size();i++){
			float x = values.get(i)/total;
			float y = 1/schedule.size();
			if (values2.get(i) > tresshold) {
				newSchedule.add(schedule.get(i));
				//System.out.println("hello");
			}
			else if (values2.get(i) == tresshold){
				tmp = schedule.get(i);
			}
		}
		if (newSchedule.size() == 0){
			newSchedule.add(tmp);
			schedule = newSchedule;
		}
		else if (newSchedule.size() > 10000){
			for (int i=0;i<=10000;i+=2){
				newSchedule2.add(newSchedule.get(i));
			}
			schedule = newSchedule2;
		}
		else {
			schedule = newSchedule;
		}
		//System.out.println("after " + schedule.size());
	}
	
//	public void animasiSolusi(String s, gui g){
//		
//		switch (s){
//		case "0" :{
//			g.lblNewLabel.setVisible(false); // mall
//			g.lblNewLabel_1.setVisible(false); // university
//			g.lblNewLabel_2.setVisible(false); // cafe
//			g.lblNewLabel_3.setVisible(false); // prerequisite kado
//			g.lblNewLabel_4.setVisible(false); // rumah cewe
//			g.lblNewLabel_5.setVisible(false); // gym
//			g.lblNewLabel_6.setVisible(true); // *ga ngapa2in*
//			break;}
//		case "1" :
//		case "2" :
//		case "3" :
//		case "4" :
//		case "5" :
//		case "6" :
//		case "7" :
//		case "8" :
//		case "9" : {
//			g.lblNewLabel.setVisible(false); // mall
//			g.lblNewLabel_1.setVisible(false); // university
//			g.lblNewLabel_2.setVisible(false); // cafe
//			g.lblNewLabel_3.setVisible(false); // prerequisite kado
//			g.lblNewLabel_4.setVisible(true); // rumah cewe
//			g.lblNewLabel_5.setVisible(false); // gym
//			g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//			break;
//		}
//		case "m" :{
//			g.lblNewLabel.setVisible(true); // mall
//			g.lblNewLabel_1.setVisible(false); // university
//			g.lblNewLabel_2.setVisible(false); // cafe
//			g.lblNewLabel_3.setVisible(false); // prerequisite kado
//			g.lblNewLabel_4.setVisible(false); // rumah cewe
//			g.lblNewLabel_5.setVisible(false); // gym
//			g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//			break;}
//		case "g" :{
//			g.lblNewLabel.setVisible(false); // mall
//			g.lblNewLabel_1.setVisible(false); // university
//			g.lblNewLabel_2.setVisible(false); // cafe
//			g.lblNewLabel_3.setVisible(false); // prerequisite kado
//			g.lblNewLabel_4.setVisible(false); // rumah cewe
//			g.lblNewLabel_5.setVisible(true); // gym
//			g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//			break;}
//		case "c" :{
//			g.lblNewLabel.setVisible(false); // mall
//			g.lblNewLabel_1.setVisible(false); // university
//			g.lblNewLabel_2.setVisible(true); // cafe
//			g.lblNewLabel_3.setVisible(false); // prerequisite kado
//			g.lblNewLabel_4.setVisible(false); // rumah cewe
//			g.lblNewLabel_5.setVisible(false); // gym
//			g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//			break;}
//		case "u " :{
//			g.lblNewLabel.setVisible(false); // mall
//			g.lblNewLabel_1.setVisible(true); // university
//			g.lblNewLabel_2.setVisible(false); // cafe
//			g.lblNewLabel_3.setVisible(false); // prerequisite kado
//			g.lblNewLabel_4.setVisible(false); // rumah cewe
//			g.lblNewLabel_5.setVisible(false); // gym
//			g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//			break;
//		
//		}
//		case "A":
//		case "B":
//		case "C":
//		case "D":
//		case "E":
//		case "F":
//		case "G":
//		case "H":
//		case "I":
//		case "J":
//		case "K":
//		case "L":
//		case "M":
//		case "N":
//		case "O":
//		case "P":
//		case "Q":
//		case "R":
//		case "S":
//		case "T":
//		case "U":
//		case "V":
//		case "W":
//		case "X":
//		case "Y":
//		case "Z": {
//			g.lblNewLabel.setVisible(false); // mall
//			g.lblNewLabel_1.setVisible(false); // university
//			g.lblNewLabel_2.setVisible(false); // cafe
//			g.lblNewLabel_3.setVisible(true); // prerequisite kado
//			g.lblNewLabel_4.setVisible(false); // rumah cewe
//			g.lblNewLabel_5.setVisible(false); // gym
//			g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//			break;
//		}
//		}
//	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI a = new GUI();
					ai frame = new ai(a);
					String res;
					//gui g = new gui();
					List<Nanto> nantos;
					nantos = new ArrayList<Nanto>();
					//int populasiAwal = 100000;
					int populasiAwal = 10000;
					
					for (int i=0;i < populasiAwal;i++){
						
						Nanto n = new Nanto();
//						n.generatejadwalnanto();
						n.jadwalnanto = Myvalidator.GeraldiGenerator();
						nantos.add(n);
						//System.out.println("Jadwal : "+nantos.get(i).jadwalnanto);
					}
					
					schedule = new ArrayList<String>();
					
					Myvalidator myvalidator = new Myvalidator();
					
					//myvalidator.BuildValidArray();
					for (int i=0;i<populasiAwal;i++){
						
						List<Integer> fault = myvalidator.Check(nantos.get(i).jadwalnanto);
						//System.out.println("====");
						if(fault.size()==0){
							
							//System.out.println(fault.size() + " " + nantos.get(i).jadwalnanto);
							schedule.add(nantos.get(i).jadwalnanto);
							threshold = myvalidator.MostMinimumTime[NumberOfCandidate-1];

							//System.out.println("Englightmen score : " + myvalidator.enlightmentscore);
							//System.out.println("Enlightment :" + myvalidator.counternlight(nantos.get(i).jadwalnanto));
							
							//System.out.println(fault.size() + " " + nantos.get(i).jadwalnanto);

						}
						System.out.println(i + " generated");
					}
					
					int i = 1;
					
					while (schedule.size() > 1 && i <= 100){
						ai.crossOver();
						ai.mutation(myvalidator);
						//System.out.println("Checkpoint");
						ai.elimination(myvalidator);
						//System.out.println(i + " " + schedule.size());
						i++;
						System.out.println(i + " GA step processed");
					}
					
					 res = "";
					int max = 0;
					if (schedule.size() > 1){						
						for (int j=0;j<schedule.size();j++){
							int val = myvalidator.countenlight(schedule.get(i));
							if (val > max){
								res = schedule.get(i);
								max = val;
							}
						}
					}
					else {
						res = schedule.get(0);
						max = myvalidator.countenlight(res);
					}
					System.out.println("Maximum : " + max + " " + res);
					
					//String anim; // buat dipecah di proses animasi
					GUI g = new GUI(res);
					g.setVisible(true);
					
					
					//for (int x=0;x<res.length();x++){
						//anim = res.substring(x, x);
						
//						try{
//							
//						    Thread.sleep(50);
//						    g.lblNewLabel.setVisible(false); // mall
//							g.lblNewLabel_1.setVisible(false); // university
//							g.lblNewLabel_2.setVisible(false); // cafe
//							g.lblNewLabel_3.setVisible(false); // prerequisite kado
//							g.lblNewLabel_4.setVisible(false); // rumah cewe
//							g.lblNewLabel_5.setVisible(false); // gym
//							g.lblNewLabel_6.setVisible(false); // *ga ngapa2in*
//							
//							Thread.sleep(50);
//							g.animasiSolusi(anim);
//						}catch(Exception e)
//						{
//						   System.out.println("Exception caught");
//						}
					
					//}
					
					ai aiframe = new ai(g);
					aiframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
	}
}