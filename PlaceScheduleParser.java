import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class PlaceScheduleParser{

	static final int NumberOfHours = 7 * 12;
	static final int NumberOfPlaces = 4;
	
	//contains the schedule, where
	//the first bracket represents the place (mall, gymnasium, cafe, university)
	//the second bracket represents the hour (10.00 to 21.00)
	public static int[][] schedule;
	
	//ctor
	public PlaceScheduleParser(){
		schedule = new int[NumberOfPlaces][NumberOfHours];
	}
	
	/*methods*/
	//read schedule from file to ScheduleArray
	public void ReadFile(){
		int rowcounter = 0;
		//read the text file
		String strschedule = "";
		String linereader;
		try{
		   BufferedReader buffreader = new BufferedReader(new FileReader("PlaceSchedule.txt"));
		   //read the text file
		   while((linereader = buffreader.readLine()) != null){
				strschedule = linereader;
				//store into ScheduleArray
				for(int j = 0; j < NumberOfHours; j++){
					if(strschedule.charAt(j) == '1'){
						schedule[rowcounter][j] = 1;
					}
				}
				//go to the next candidate's schedule
				rowcounter++;
		   }
		}
		catch(IOException e){
		   System.out.println("File not found");
		}
	}
	
	
	
	//print the schedule (for testing purposes)
	public void ShowSchedule(){
		System.out.println("Jadwal Tempat dari Senin sampai Minggu");
		for(int i = 0; i < NumberOfPlaces; i++){
			switch(i){
				case 0 : System.out.print("Mall       ");break;
				case 1 : System.out.print("Gymnasium  ");break;
				case 2 : System.out.print("Cafe       ");break;
				case 3 : System.out.print("University ");break;
			}
			for(int j = 0; j < NumberOfHours; j++){
				System.out.print(schedule[i][j]);
				if((j+1) % 7 == 0){
					System.out.print("|");
				}
			}
			System.out.println("");
		}
	}
	
//	public void displayschedule(){
//		for (int i = 0; i < 4; i++) {
//			System.out.print("\n"+i+">>>");
//			for (int j = 0; j < NumberOfHours; j++) {
//				System.out.print(schedule[i][j]);
//			}
//		}
//	}
	
	//check if a place is available at a time
	//placeCode are [m,g,c,u]
	//timeCode represents time in a week (0-83)
	public boolean isCandidateAvailable(char placeCode, int timeCode){
		int placeCode_int = 0;
		switch(placeCode){
			case 'm':{
				placeCode_int = 0;
				break;
			}
			case 'g':{
				placeCode_int = 1;
				break;
			}
			case 'c':{
				placeCode_int = 2;
				break;
			}
			case 'u':{
				placeCode_int = 3;
				break;
			}
		}
		if(schedule[placeCode_int][timeCode] == 1){
			return true;
		}
		else{
			return false;
		}
	}

	
	//public static void main(String args[]){
		//PlaceScheduleParser psp = new PlaceScheduleParser();
		//psp.ReadFile();
		//psp.ShowSchedule();
		//System.out.println("ahay ahay "+psp.isCandidateAvailable('m',0));
	//}
	
}
